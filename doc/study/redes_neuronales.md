# Red Neuronal

Una red neuronal es un **modelo** muy simple que **emula** el funcionamiento
del **cerebro humano**.


El procesamiento se organiza en distintas **capas**:

- **Capa de Entrada**: datos de entrada (`inputs`), en ocasiones son la salida de otra neurona.
- **Capa/s Ocultas**: cálculos
- **Capa de Salida**: salida de los calculos, puede ser una toma de decisión o la entrada a otra capa.


En redes neuronales se utilizan **tensores** como datos de entrada y también de salida.
*Por ejemplo, una imagen que la separamos en pixeles de 5x5 y los guardamos todos en un tensor de forma lineal*


## Redes Profundas

Para la elección de un tipo de red u otra nos vamos a basar en lo 
que vayamos a construir, es decir, vamos a **clasificar** datos o buscar
**patrones** en los datos.

Tener en cuenta también si utilizaremos aprendizaje sin supervisión


#### CNN  - Convolucionales

#### RNNs - Recurrentes

En este tipo de redes los datos pueden moverse a cualquier dirección.

Se utilizan para el **procesamiento del lenguaje natural**

Las RNN guardan información de todos los anteriores cálculos y
la salida de las capas se basan en los anteriores cálculos.

#### GANs - Antagónicas

#### RBN  - Redes de Boltzman Restringidas (Autoencoders) 

Red de **dos capas** poco profunda que soluciona el problema de los
**gradientes de fuga**.

1. Capa **Visible**
1. Capa **Oculta**

Cada nodo de la capa visible está conectado a todos los nodos de la capa
oculta.

Este tipo de redes codifican los datos de entrada como **vectores**.

#### DBNs - Creencias Profundas 



## Elegir tipo de Red

- Para procesar **texto** se utiliza una red **recurrente** o red de **tensor neuronal recursivo** *RNTN*
- Para procesas **imágenes** se utiliza la **DBN** o **CNN**
- Para reconocimiento de **objetos** se utiiliza **RNTN** o **CNN**
- Para reconocimiento de **voz** se utiliza **RNN**


## Problemas

El gran problema de las Redes Neuronales siempre ha sido
el **entrenamiento**.

Si se entrena una red con el metodo de **propagación**
surge un problema denominado **gradiente de fuga**.

