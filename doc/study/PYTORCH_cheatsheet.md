<span style="color:blue">import torch</span> --> importar libreria Pytorch

<span style="color:blue">import torchvision</span> --> es un paquete con una recopilación de datos o
datasets que nos permite entrenar y testear nuestra red neuronal de
visión, es decir solo con imagenes

-   <span style="color:blue">from torchvision import transforms, datasets</span> --> importamos los
    datasets y transformaciones ( para poder trabajar con las muestras
    debemos de aplicar una transformación a todas ellas, prepararlas,
    no puedes comerte un pollo crudo, tienes que cocinarlo, para ello
    se utilizan las transformaciones) de torchvision

-   Al crear una red neuronal disponemos de dos grandes conjuntos de
    datos, uno de entrenamiento y otro de testeo o prueba. Los datos
    de entrenamiento se utilizan para entrenar a nuestra red mientras
    que los de testeo sirven para confirmar que funciona nuestra red
    neuronal.

-   <span style="color:blue">MNIST</span> --> es un modelo o dataset con 60.000 imágenes para entrenamiento
    y 10.000 imágenes para test de números del 1-10 escritos a mano.
    Su sintaxis es la siguiente:

    -   <span style="color:blue">variable = datasets.MNIST("ruta de descarga del dataset",
        train=TRUE/FALSE, donwload=TRUE/FALSE,
        transform=transforms.Compose(\[transforms.ToTensor()\]))</span> -->
        con train indicamos si es un dataset de entrenamiento o
        testeo, donwload nos permite descargar el dataset, y con
        transform aplicamos una transformación a las muestras, en este
        caso las convertimos a tensores

    -   Una vez descargado el dataset necesitaremos un Dataloader (es un
        generador que nos proporcionará las muestras en grupos de un
        tamaño dado) su sintaxis es la siguiente:

        -   <span style="color:blue">variable = torch.utils.data.DataLoader(train/test,
            batch_size=lote de muestras, shuffle=True/False)</span> --> train
            o test dependiendo de si el lote es de entrenamiento o de
            testeo, el tamaño de batch es el tamaño de cada lote, es
            la cantidad de muestras que le daremos a la red neuronal
            cada vez, shuffle reordena aleatoriamente el dataset en
            cada iteración

<span style="color:blue">tensor</span> --> es una matriz multidimensional

<span style="color:blue">x.shape</span> --> muestra el tamaño del tensor

Declarar variables de tensores

- <span style="color:blue">x = torch.Tensor(\[5,3\])</span>

- <span style="color:blue">y = torch.Tensor(\[2,1\])</span>

- <span style="color:blue">print(x\*y)</span> --> multiplicar matrices (esto es una puta maravilla)

Rellenar un tensor de ceros

<span style="color:blue">x = torch.zeros(\[2,5\])</span>

Rellenar un tensor con números aleatorios

<span style="color:blue">y = torch.rand(\[2,5\])</span>

Remodelar tensores (viene a se cambiar el tamaño de la matriz)

<span style="color:blue">y.view(\[1,10\])</span>

Como se puede ver estamos creando una vista del tensor *y*, igual
que MySql, para modificar *y* debemos de igualar la vista

<span style="color:blue">y = y.view(\[1,10\])</span>
