import torch
import Brain
import signal
import Connection
import os
import time

if __name__ == '__main__':

    # Variables
    # -----------------------------------------------------------------
    brain = Brain.Brain()
    dbus = Connection.Connection()
    # -----------------------------------------------------------------

    # Cargar modelo entrenado
    # -----------------------------------------------------------------
    if dbus.isPredict():
        saved_brain = torch.load("model.pt")
        brain.model = saved_brain.model.eval()
    # -----------------------------------------------------------------

    # Crear datasets
    # -----------------------------------------------------------------
    else:
        torch.save(dbus.getDbInputs(), "inputs_db.pt")
        torch.save(dbus.getDbTarget(), "target_db.pt")
    # -----------------------------------------------------------------

    # Fase de entrenamiento
    # -----------------------------------------------------------------
    if not dbus.isPredict():
        brain.train(torch.load('inputs_db.pt'),
                       torch.load('target_db.pt'), dbus.getEpochs(), dbus)
        dbus.run()
    # -----------------------------------------------------------------

    # Guardar modelo entrenado
    # -----------------------------------------------------------------
    if not dbus.isPredict():
        torch.save(brain, "model.pt")
    # -----------------------------------------------------------------

    # Fase de predicciones
    # -----------------------------------------------------------------
    while os.getppid() != 1:
        dbus.sendMove(brain.predict(dbus.getInput()))
        time.sleep(.5)
    # -----------------------------------------------------------------

    # Liberar memoria dbus
    # -----------------------------------------------------------------
    del dbus.dbus_obj
    # -----------------------------------------------------------------