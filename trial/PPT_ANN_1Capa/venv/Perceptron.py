import numpy as np

class Perceptron:

    # Variables Peso y Bias (sesgo)
    weight = []
    bias = []

    # Constructor
    def __init__ (self, size):

        # Asignar valores aleatorios
        self.weight = np.random.randn(size)
        self.bias = np.random.randn(3)

        # Transposicionar matriz a 3x3
        self.weight = np.array(self.weight).reshape(3,3).T

    # Función de Propagación
    def annOutput (self, input, weight):
        return np.dot(input, weight) + self.bias

    # Función Sigmoide
    def sigmoidFunction (self, input):
        return 1 / (1 + np.exp(-input))

    # Derivada de la Sigmoide
    def sigmoidDer (self, input):
        return self.sigmoidFunction(input) * (1-self.sigmoidFunction(input))

    # Fase de entrenamiento del Perceptron simple
    def training (self, inputs, gTruth, epochs, learningRate):

        for epoch in range(epochs):
            # Calcular la salida de la red para el input dado (Función de Propagación)
            output = self.annOutput(inputs, self.weight)

            # Aplicar la función sigmoide para la salida anterior
            sig = self.sigmoidFunction(output)

            # Calcular el Error en el valor de predicción
            error = sig - gTruth

            # Sumatorio error (comprobar la evolución)
            sumError = error.sum()
            print("Valor del Error: ", sumError)

            # Calcular el valor de actualización de los pesos mediante el Descenso de Gradiante (Modo Batch)
            # Propagacion hacia atras (backpropagation)
            # Calcular la 1º derivada
            der = self.sigmoidDer(sig)
            # Calcular la 2º derivada (Multiplicar derivadas individuales)
            derv = error * der

            # Calcular la 3º derivada (Multiplicar con la tercera derivada individual)
            dervFinal = np.dot(inputs, derv)  # se multiplica por los todo el dataset (Modo Batch)

            # Calcular el Error en el valor deredicción mediante Mean Squared Error (MSE, Error Cuadratico Medio)

            # Actualizar pesos
            self.weight -= learningRate * dervFinal

            # Actualizar los valores del sesgo (bias)
            for i in derv:
                self.bias -= learningRate * i


    # Calcular predicciones
    def predict (self, input):
        return self.sigmoidFunction(self.annOutput(input, self.weight))