class Reglas:

    # Definir Dataset y el target o etiquetas para dicho Dataset
    #         Piedra,  Papel,  Tijera
    inputs = [1,0,0], [0,1,0], [0,0,1]
    gTruth = [0,1,0], [0,0,1], [1,0,0]

    # Mapear las opciones para mostar resultados
    def mapPlayerOption(self, option):

        if option == [1,0,0]:
            resul = "Piedra"
        elif option == [0,1,0]:
            resul = "Papel"
        elif option == [0,0,1]:
            resul = "Tijera"

        return resul

    def mapAnnOption(self, option):

        if option.item(0) >= 0.99:
            resul = "Piedra"
        if option.item(1) >= 0.99:
            resul = "Papel"
        if option.item(2) >= 0.99:
            resul = "Tijera"

        return resul