class Reglas:

    # Definir Dataset y el target o etiquetas para dicho Dataset
    #         Piedra,  Papel,  Tijera
    inputs = [1,0,0], [0,1,0], [0,0,1]
    gTruth = [0,1,0], [0,0,1], [1,0,0]

    # Mapear las opciones para mostar resultados
    def mapPlayerOption(self, option):

        if option == [1,0,0]:
            resul_player = "Piedra"
        elif option == [0,1,0]:
            resul_player = "Papel"
        elif option == [0,0,1]:
            resul_player = "Tijera"

        return resul_player

    def mapAnnOption(self, option):

        resul_ann = ""

        if option.item(0)>= .90:
            resul_ann = "Piedra"
        if option.item(1) >= .90:
            resul_ann = "Papel"
        if option.item(2) >= .90:
            resul_ann = "Tijera"

        return resul_ann
