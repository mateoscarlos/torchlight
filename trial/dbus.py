import dbus

bus = dbus.SessionBus()
obj = bus.get_object('org.torchlight.Inputs', '/inputs')


def getAllMovements():
    movements = obj.get_dbus_method('getInputSize',
                                    dbus_interface='local.controller.Controller')()
    return movements


def getXandY():
    total = getAllMovements()

    for i in range(total):
        x = obj.get_dbus_method('sendInputX', dbus_interface='local.controller.Controller')(i)
        y = obj.get_dbus_method('sendInputY', dbus_interface='local.controller.Controller')(i)
        print(x, y)


def sendNewDirection(direction):
    obj.get_dbus_method('newDirection', dbus_interface='local.controller.Controller')(direction)

if __name__ == '__main__':
    #sendNewDirection(2)
    print(getAllMovements())