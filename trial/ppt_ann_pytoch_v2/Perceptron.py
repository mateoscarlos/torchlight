import torch
import numpy as np

'''
    lh -> layer hidden
    lo -> layer output
    Descenso de Gradiante (Modo Batch)
'''

class Perceptron:

    D_in, H, D_out = 3, 3, 3
    l = []

    # Constructor
    def __init__ (self):
        # Sequential actua como un contenedor secuencial, al llamar a este variable se van a ejecutar los modulos en el orden que se han pasado al constructor
        # Linear define una capa del modelo de la red, aplica una transformacion lineal, viene a ser la funcion de propagacion de esa capa (entrada * salida + bias)
        self.model = torch.nn.Sequential(
            torch.nn.Linear(self.D_in, self.H, bias = True),                                                            # bias=True por defecto
            torch.nn.ReLU(),                                                                                            # función de activacion
            torch.nn.Linear(self.H, self.D_out, bias=True))

    # Fase de entrenamiento
    def training (self, inputs, target, epochs, lr):
        for epoch in range(epochs):
            output = self.model(torch.from_numpy(np.array(inputs)).float())

            error = output - torch.as_tensor(target)
            print (error.sum())

            # Ponemos a 0 los gradiantes
            self.model.zero_grad()

            # Calculamos todos los gradiantes automaticamente
            error.mean().backward()

            # Actualizamos los pesos
            with torch.no_grad():
                for weight in self.model.parameters():
                    weight -= lr * weight.grad

    # Calcular predicciones
    def predict (self, inputs):
        print (self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid())
        return self.model(torch.from_numpy(np.array(inputs)).float()).sigmoid().detach().numpy()
