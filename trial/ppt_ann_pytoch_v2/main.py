import torch
import numpy as np
from Perceptron import Perceptron
from random import choice
from Reglas import Reglas

import numpy as np

'''
    Descripcion -> Creación de un Modelo de Inteligencia Artificial capaz de aprender a jugar a Piedra, Papel, Tijera
    Tipo de Modelo -> Perceptrón Multicapa
    Tipo de aprendizaje -> Supervisado
    Capas del Modelo -> 3 Capas: 3 Neuronas en la capa de entrada + neurona bias
                                 3 neuronas en la capa de oculta  + neurona bias
                                 3 neuronas en la capa de salida
    Versión -> v2.0
'''

if __name__ == '__main__':

    # Construir Dataset y Target
    game = Reglas()

    # Construir el modelo
    ann = Perceptron()
    '''
    z = [1, 0, 0]
    inputs = torch.as_tensor(z)
    a = [[[1,1],[1,1]], [[1,1],[1,1]]]
    b = [[1, 0, 0]]
    x = torch.tensor(np.random.normal(scale=1, size=(3,1)))
    y = torch.FloatTensor(a)
    print (np.array(inputs).shape, np.array(z).shape, np.array(a).shape, a)
    '''
    #Entrenamiento
    ann.training(game.inputs, game.gTruth, 2000, 0.1)

    # Pesos y Predicciones
    '''
    print("\nPesos Finales Capa Oculta:\n", ann.weight_lh)
    print("\nPesos Finales Capa Salida:\n", ann.weight_lo)
    print("\nValor del Bias Capa Oculta:\n", ann.bias_lh)
    print("\nValor del Bias Capa Salida:\n", ann.bias_lo)
    print("\nPredicciones:")
    '''

    for i in range(10):
        playerOption = choice(game.inputs)
        print(" Jugador: %s; Modelo: %s" % (game.mapPlayerOption(playerOption), game.mapAnnOption(ann.predict(playerOption))))
